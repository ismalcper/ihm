-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-11-2019 a las 05:32:38
-- Versión del servidor: 10.1.40-MariaDB
-- Versión de PHP: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `integr73_ihm`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE `administrador` (
  `id_administrador` varchar(6) NOT NULL,
  `id_persona` varchar(6) DEFAULT NULL,
  `profesion` varchar(30) DEFAULT NULL,
  `grado_academico` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`id_administrador`, `id_persona`, `profesion`, `grado_academico`) VALUES
('ADM001', 'PER002', 'PROFESOR', 'DOCTORADO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno`
--

CREATE TABLE `alumno` (
  `cuenta` int(7) NOT NULL,
  `id_persona` varchar(6) DEFAULT NULL,
  `id_carrera` varchar(6) DEFAULT NULL,
  `semestre` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `alumno`
--

INSERT INTO `alumno` (`cuenta`, `id_persona`, `id_carrera`, `semestre`) VALUES
(1220804, 'PER001', 'ING001', 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carrera`
--

CREATE TABLE `carrera` (
  `id_carrera` varchar(6) NOT NULL,
  `nombre_carrera` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `carrera`
--

INSERT INTO `carrera` (`id_carrera`, `nombre_carrera`) VALUES
('ING001', 'INGENIERIA EN COMPUTACION');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `docente`
--

CREATE TABLE `docente` (
  `id_docente` varchar(6) NOT NULL,
  `id_persona` varchar(6) DEFAULT NULL,
  `profesion` varchar(30) DEFAULT NULL,
  `grado_academico` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `docente`
--

INSERT INTO `docente` (`id_docente`, `id_persona`, `profesion`, `grado_academico`) VALUES
('DOC001', 'PER003', 'PROFESOR ASIGNATURA', 'DOCTORADO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `docente_materia`
--

CREATE TABLE `docente_materia` (
  `id_docente_materia` int(11) NOT NULL,
  `id_materia` varchar(6) DEFAULT NULL,
  `id_docente` varchar(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `docente_materia`
--

INSERT INTO `docente_materia` (`id_docente_materia`, `id_materia`, `id_docente`) VALUES
(2, 'MAT001', 'DOC001');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materia`
--

CREATE TABLE `materia` (
  `id_materia` varchar(6) NOT NULL,
  `nombre_materia` varchar(50) DEFAULT NULL,
  `id_carrera` varchar(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `materia`
--

INSERT INTO `materia` (`id_materia`, `nombre_materia`, `id_carrera`) VALUES
('MAT001', 'INTERACCION HOMBRE MAQUINA', 'ING001');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `material`
--

CREATE TABLE `material` (
  `id_material` varchar(6) NOT NULL,
  `nombre_materiaL` varchar(20) DEFAULT NULL,
  `descripción` varchar(100) DEFAULT NULL,
  `cantidad_existente` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `material`
--

INSERT INTO `material` (`id_material`, `nombre_materiaL`, `descripción`, `cantidad_existente`) VALUES
('MTR001', 'XBOX', 'PAQUETE XBOX CON DOS CONTROLES', 10),
('MTR002', 'TELEFONO ANALOGO', 'TELECONO CON CABLE', 10),
('MTR003', 'OCULUS RIFT', 'PAQUETE 2 MANDOS, 2 SENSORES, 1 LENTES', 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `id_persona` varchar(6) NOT NULL,
  `nombre` varchar(30) DEFAULT NULL,
  `apellido_paterno` varchar(30) DEFAULT NULL,
  `apellido_materno` varchar(30) DEFAULT NULL,
  `contrasena` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`id_persona`, `nombre`, `apellido_paterno`, `apellido_materno`, `contrasena`) VALUES
('PER001', 'RODOLFO', 'QUEZADA', 'HUITRON', 'RQH12345'),
('PER002', 'PERSONA1', 'AP1', 'AP1', 'PRUEBA1'),
('PER003', 'PERSONA2', 'AP2', 'AP2', 'PRUEBA2'),
('PER004', 'PERSONA3', 'AP3', 'AP3', 'PRUEBA3');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prestamo`
--

CREATE TABLE `prestamo` (
  `id_prestamo` int(11) NOT NULL,
  `id_docente` varchar(6) DEFAULT NULL,
  `cuenta_alumno` int(7) DEFAULT NULL,
  `id_materia` varchar(6) DEFAULT NULL,
  `fecha_inicio` timestamp(5) NULL DEFAULT NULL,
  `fecha_fin` timestamp(5) NULL DEFAULT NULL,
  `estado_solicitud` enum('SOLICITADO','APROBADO','RECHAZADO','ENTREGADO') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `prestamo`
--

INSERT INTO `prestamo` (`id_prestamo`, `id_docente`, `cuenta_alumno`, `id_materia`, `fecha_inicio`, `fecha_fin`, `estado_solicitud`) VALUES
(3, 'DOC001', 1220804, 'MAT001', '2019-11-21 10:58:14.00000', NULL, 'SOLICITADO'),
(4, 'DOC001', 1220804, 'MAT001', '2019-11-21 10:59:39.00000', NULL, 'SOLICITADO'),
(5, 'DOC001', 1220804, 'MAT001', '2019-11-21 11:03:37.00000', NULL, 'SOLICITADO'),
(6, 'DOC001', 1220804, 'MAT001', '2019-11-21 11:04:26.00000', NULL, 'SOLICITADO'),
(7, 'DOC001', 1220804, 'MAT001', '2019-11-21 11:08:55.00000', NULL, 'SOLICITADO'),
(8, 'DOC001', 1220804, 'MAT001', '2019-11-21 11:10:08.00000', NULL, 'SOLICITADO'),
(9, 'DOC001', 1220804, 'MAT001', '2019-11-21 11:11:19.00000', NULL, 'SOLICITADO'),
(10, 'DOC001', 1220804, 'MAT001', '2019-11-21 11:11:52.00000', NULL, 'SOLICITADO'),
(11, 'DOC001', 1220804, 'MAT001', '2019-11-21 11:13:34.00000', NULL, 'SOLICITADO'),
(12, 'DOC001', 1220804, 'MAT001', '2019-11-21 11:15:24.00000', NULL, 'SOLICITADO'),
(13, 'DOC001', 1220804, 'MAT001', '2019-11-21 11:16:51.00000', NULL, 'SOLICITADO'),
(14, 'DOC001', 1220804, 'MAT001', '2019-11-21 11:16:56.00000', NULL, 'SOLICITADO'),
(15, 'DOC001', 1220804, 'MAT001', '2019-11-21 11:20:00.00000', NULL, 'SOLICITADO'),
(16, 'DOC001', 1220804, 'MAT001', '2019-11-21 11:20:22.00000', NULL, 'SOLICITADO'),
(17, 'DOC001', 1220804, 'MAT001', '2019-11-21 11:21:42.00000', NULL, 'SOLICITADO'),
(18, 'DOC001', 1220804, 'MAT001', '2019-11-21 11:22:04.00000', NULL, 'SOLICITADO'),
(19, 'DOC001', 1220804, 'MAT001', '2019-11-21 11:22:51.00000', NULL, 'SOLICITADO'),
(20, 'DOC001', 1220804, 'MAT001', '2019-11-21 11:23:43.00000', NULL, 'SOLICITADO'),
(21, 'DOC001', 1220804, 'MAT001', '2019-11-21 11:24:35.00000', NULL, 'SOLICITADO'),
(22, 'DOC001', 1220804, 'MAT001', '2019-11-21 11:24:57.00000', NULL, 'SOLICITADO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prestamo_administrador`
--

CREATE TABLE `prestamo_administrador` (
  `id_prestamo_administrador` int(11) NOT NULL,
  `id_prestamo` int(11) DEFAULT NULL,
  `id_administrador` varchar(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prestamo_material`
--

CREATE TABLE `prestamo_material` (
  `id_prestamo_material` int(11) NOT NULL,
  `id_material` varchar(6) DEFAULT NULL,
  `id_prestamo` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`id_administrador`),
  ADD KEY `id_persona` (`id_persona`);

--
-- Indices de la tabla `alumno`
--
ALTER TABLE `alumno`
  ADD PRIMARY KEY (`cuenta`),
  ADD KEY `id_carrera` (`id_carrera`),
  ADD KEY `id_persona` (`id_persona`);

--
-- Indices de la tabla `carrera`
--
ALTER TABLE `carrera`
  ADD PRIMARY KEY (`id_carrera`);

--
-- Indices de la tabla `docente`
--
ALTER TABLE `docente`
  ADD PRIMARY KEY (`id_docente`),
  ADD KEY `id_persona` (`id_persona`);

--
-- Indices de la tabla `docente_materia`
--
ALTER TABLE `docente_materia`
  ADD PRIMARY KEY (`id_docente_materia`),
  ADD KEY `id_materia` (`id_materia`),
  ADD KEY `id_docente` (`id_docente`);

--
-- Indices de la tabla `materia`
--
ALTER TABLE `materia`
  ADD PRIMARY KEY (`id_materia`),
  ADD KEY `id_carrera` (`id_carrera`);

--
-- Indices de la tabla `material`
--
ALTER TABLE `material`
  ADD PRIMARY KEY (`id_material`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`id_persona`);

--
-- Indices de la tabla `prestamo`
--
ALTER TABLE `prestamo`
  ADD PRIMARY KEY (`id_prestamo`),
  ADD KEY `id_docente` (`id_docente`),
  ADD KEY `cuenta_alumno` (`cuenta_alumno`),
  ADD KEY `id_materia` (`id_materia`);

--
-- Indices de la tabla `prestamo_administrador`
--
ALTER TABLE `prestamo_administrador`
  ADD PRIMARY KEY (`id_prestamo_administrador`),
  ADD KEY `id_administrador` (`id_administrador`),
  ADD KEY `id_prestamo` (`id_prestamo`);

--
-- Indices de la tabla `prestamo_material`
--
ALTER TABLE `prestamo_material`
  ADD PRIMARY KEY (`id_prestamo_material`),
  ADD KEY `id_material` (`id_material`),
  ADD KEY `id_prestamo` (`id_prestamo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `docente_materia`
--
ALTER TABLE `docente_materia`
  MODIFY `id_docente_materia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `prestamo`
--
ALTER TABLE `prestamo`
  MODIFY `id_prestamo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `prestamo_administrador`
--
ALTER TABLE `prestamo_administrador`
  MODIFY `id_prestamo_administrador` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `prestamo_material`
--
ALTER TABLE `prestamo_material`
  MODIFY `id_prestamo_material` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD CONSTRAINT `administrador_ibfk_1` FOREIGN KEY (`id_persona`) REFERENCES `persona` (`id_persona`);

--
-- Filtros para la tabla `alumno`
--
ALTER TABLE `alumno`
  ADD CONSTRAINT `alumno_ibfk_1` FOREIGN KEY (`id_carrera`) REFERENCES `carrera` (`id_carrera`),
  ADD CONSTRAINT `alumno_ibfk_2` FOREIGN KEY (`id_persona`) REFERENCES `persona` (`id_persona`);

--
-- Filtros para la tabla `docente`
--
ALTER TABLE `docente`
  ADD CONSTRAINT `docente_ibfk_1` FOREIGN KEY (`id_persona`) REFERENCES `persona` (`id_persona`);

--
-- Filtros para la tabla `docente_materia`
--
ALTER TABLE `docente_materia`
  ADD CONSTRAINT `docente_materia_ibfk_1` FOREIGN KEY (`id_materia`) REFERENCES `materia` (`id_materia`),
  ADD CONSTRAINT `docente_materia_ibfk_2` FOREIGN KEY (`id_docente`) REFERENCES `docente` (`id_docente`);

--
-- Filtros para la tabla `materia`
--
ALTER TABLE `materia`
  ADD CONSTRAINT `materia_ibfk_1` FOREIGN KEY (`id_carrera`) REFERENCES `carrera` (`id_carrera`);

--
-- Filtros para la tabla `prestamo`
--
ALTER TABLE `prestamo`
  ADD CONSTRAINT `prestamo_ibfk_1` FOREIGN KEY (`id_docente`) REFERENCES `docente` (`id_docente`),
  ADD CONSTRAINT `prestamo_ibfk_2` FOREIGN KEY (`cuenta_alumno`) REFERENCES `alumno` (`cuenta`),
  ADD CONSTRAINT `prestamo_ibfk_5` FOREIGN KEY (`id_materia`) REFERENCES `materia` (`id_materia`);

--
-- Filtros para la tabla `prestamo_administrador`
--
ALTER TABLE `prestamo_administrador`
  ADD CONSTRAINT `prestamo_administrador_ibfk_1` FOREIGN KEY (`id_administrador`) REFERENCES `administrador` (`id_administrador`),
  ADD CONSTRAINT `prestamo_administrador_ibfk_2` FOREIGN KEY (`id_prestamo`) REFERENCES `prestamo` (`id_prestamo`);

--
-- Filtros para la tabla `prestamo_material`
--
ALTER TABLE `prestamo_material`
  ADD CONSTRAINT `prestamo_material_ibfk_1` FOREIGN KEY (`id_material`) REFERENCES `material` (`id_material`),
  ADD CONSTRAINT `prestamo_material_ibfk_2` FOREIGN KEY (`id_prestamo`) REFERENCES `prestamo` (`id_prestamo`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
