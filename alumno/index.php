<?php
require "../conexion.php";
session_start();
if(isset($_SESSION['alumno'])){
$admin=$_SESSION['alumno'];
   
}else{
    header('Location: ../index.php');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laboratorio IHM</title>
    <link rel="stylesheet" href="../fontawesome/css/all.min.css">
    <link rel="stylesheet" href="../bootstrap-4.3.1/css/bootstrap.css">
    <link rel="stylesheet" href="../css/styles.css">
</head>

<body>
    <img src="../img/bannerFI.png" alt="" srcset="">
    <nav class="navbar navbar-expand-lg  barra ">


        <div class="collapse navbar-collapse nav-tabs">
            <ul class="navbar-nav mr-auto ">
                <li class="nav-item">
                    <a class="nav-link active" href="index.php">Home</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link " href="prestamo.php">Prestamo de material</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Registro de acceso</a>
                </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">
                <a class="nav-link" href="../cerrar.php">Salir</a>
            </form>
        </div>
    </nav>
    <div>
        <div class="container">
            <div class="row">
                <div class="col-md-4 formulario">

                    <?php
                        $sql = "SELECT * FROM alumno INNER JOIN persona ON persona.id_persona=alumno.id_persona INNER JOIN carrera ON alumno.id_carrera=carrera.id_carrera where cuenta ='$admin'";
                        $conn=conectar();
                        $result=mysqli_query($conn,$sql); 
                        while ($row=mysqli_fetch_array($result)) {
                        echo '<div class="row">
                            <img src="../img/usuario.png" alt="" srcset="">
                        </div>
                        <div class="row">
                            <h3>No. Cuenta: '.$row['cuenta'].'</h3>
                        </div>
                        <br>
                        <div class="row">
                            <h5>Nombre: '.$row['nombre'].' '.$row['apellido_paterno'].' '.$row['apellido_materno'].'</h5>
                        </div>
                        <br>
                        <div class="row">
                            <h5>Carrera: '.$row['nombre_carrera'].'</h5>
                        </div>
                        <br>
                        <div class="row">
                            <h5>Semestre: '.$row['semestre'].'</h5>
                        </div>';
                
                         }
                        $conn->close();
          
                    ?>
                </div>
                <div class="col-md-4 botones">
                    <a name="" id="" class="btn " href="prestamo.php" role="button">Solicitar material</a>
                </div>
                <div class="col-md-4 botones">
                    <a name="" id="" class="btn" href="#" role="button">Registrar Acceso</a>
                </div>
            </div>
        </div>
        <br>
        <br>
        <br>
        <br>
        <div id="footer">
        </div>
    </div>

    <script src="../bootstrap-4.3.1/js/bootstrap.js"></script>
    <script src="../js/jquery-3.4.1.js"></script>

    <script>
    $(document).ready(function() {
        $("#footer").load("../footer.html");
    });
    </script>
</body>

</html>