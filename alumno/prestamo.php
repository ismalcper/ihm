<?php
require "../conexion.php";
session_start();
if(isset($_SESSION['alumno'])){
$admin=$_SESSION['alumno'];
   
}else{
    header('Location: ../index.php');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laboratorio IHM</title>
    <link rel="stylesheet" href="../fontawesome/css/all.min.css">
    <link rel="stylesheet" href="../bootstrap-4.3.1/css/bootstrap.css">
    <link rel="stylesheet" href="../css/styles.css">
</head>

<body>
    <img src="../img/bannerFI.png" alt="" srcset="">
    <nav class="navbar navbar-expand-lg  barra ">


        <div class="collapse navbar-collapse nav-tabs">
            <ul class="navbar-nav mr-auto ">
                <li class="nav-item">
                    <a class="nav-link " href="index.php">Home</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link active" href="prestamo.php">Prestamo de material</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Registro de acceso</a>
                </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">
                <a class="nav-link" href="../cerrar.php">Salir</a>
            </form>
        </div>
    </nav>
    <div>
        <div class="container" align="center">
            <div class="row">
                <div class="col-md-12">
                    <div>
                        <label for="">Prestamo de material</label>
                        <div class="col-md-10" align="right">
                            <select class="form-control" name="materiales" id="materiales">

                            </select>
                            <a name="agregar" id="agregar" class="btn btn-primary" href="#" role="button">Agregar</a>
                        </div>

                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class=" col-md-6" align="center">
                    <select class="form-control" name="profesor" id="profesor">

                    </select>
                </div>
                <div class=" col-md-6" align="center">
                    <select class="form-control" name="materia" id="materia">

                    </select>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12" align="center">
                    <table class="table table-striped table-inverse">
                        <thead class="thead-inverse">
                            <tr>
                                <th>Clave</th>
                                <th>Nombre de material</th>
                                <th>Cantidad</th>
                                <th>Eliminar</th>
                            </tr>
                        </thead>
                        <tbody id="tbl_materiales">


                        </tbody>
                    </table>
                </div>
                <div class=" col-md-12" align="right">
                    <a name="" id="solicitar" class="btn btn-success" href="#" role="button">Solicitar
                        material</a>
                </div>

            </div>
        </div>
        <br>
        <br>
        <br>
        <br>
        <div id="footer">
        </div>
    </div>

    <script src="../bootstrap-4.3.1/js/bootstrap.js"></script>
    <script src="../js/jquery-3.4.1.js"></script>
    <script src="../js/funciones_alumno.js"></script>

    <script>
    $(document).ready(function() {
        $("#footer").load("../footer.html");
        materiales("materiales");
        materias("materias");
        profesores("profesores");
        $("#agregar").click(function(e) {
            var id = $("#materiales").val();
            agregar_material(id);

        });
        $("#solicitar").click(function(e) {
            solicitar_prestamo();

        });
    });
    </script>
</body>

</html>