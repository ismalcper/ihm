function login_alumno(funcion) {
    /*  proposito: verificar al usuario como administrador
        precondiciones: 
            1) La variable funcion guarda el nombre del case en php 
            2) Los campos en cuenta y contraseña no estan vacios
        poscondiciones: 
            1) Permitimos el acceso a la pagina index.php de administradores
    */
    var cuenta = $("#cuenta").val();
    var contrasena = $("#contrasena").val();
    var cadena = "cuenta=" + cuenta + "&contrasena=" + contrasena + "&funcion=" + funcion;
    $.ajax({
        type: "POST",
        url: "alumno/funciones_alumno.php",
        data: cadena,
        success: function (response) {

            if (response == "true") {
                location.href = "alumno/index.php";
            } else {
                alert("Error de credenciales");
            }
        }
    });
}

function materiales(funcion) {
    /*  proposito: buscar todas las carrerar que se tienen en la base
        precondiciones: 
            1) La variable funcion guarda el nombre del case en php 
        poscondiciones: 
            1) Agregamos las carrera al select de carreras dentro del formulario
    */

    $.ajax({
        type: "POST",
        url: "funciones_alumno.php",
        data: "funcion=" + funcion,
        success: function (response) {
            var obj = JSON.parse(response);
            $("#materiales").empty();
            $("#materiales").append(" <option>Selecciona una opción</option>");
            $.each(obj, function (i, value) {
                $("#materiales").append('<option value="' + obj[i].id_material + '|' + obj[i].nombre_materiaL + '">' + obj[i].nombre_materiaL + '</option>');
            });
        }
    });
}

function agregar_material(id) {


    var indenti = id.substring(0, id.indexOf("|"));
    var nombre = id.substring(id.indexOf("|") + 1, id.length);
    if (indenti != "") {
        var bandera = false;
        $("#tbl_materiales tr td button").each(function (index) {
            var valor = $(this).val();
            if (valor == indenti) {
                bandera = true;
            }
        });
        if (!bandera) {
            $("#tbl_materiales").append('<tr id="fila' + indenti + '"><td scope="row">' + indenti + '</td><td>' + nombre + '</td><td><input id="cantidad' + indenti + '" type="number" min="0" max="20" class="form-control" name="" id=""aria-describedby="helpId" placeholder="" value="1"></td><td><button name="" onclick="eliminar(' + "'" + indenti + "'" + ')" class="btn btn-danger"  value="' + indenti + '" role="button">X</button></td></tr>');
        }


    }
}

function eliminar(id) {
    $("#fila" + id).remove();
}

function solicitar_prestamo() {
    var cantidad = []
    var id = [];
    $("#tbl_materiales tr td input").each(function (index) {
        var valor = $(this).val();
        cantidad.push(valor);
    });
    $("#tbl_materiales tr td button").each(function (index) {
        var valor = $(this).val();
        id.push(valor);
    });
    var profesor = $("#profesor").val();
    var materia = $("#materia").val();
    solicitar_prestamo_final(cantidad, id, profesor, materia);


}

function solicitar_prestamo_final(cantidad, id, profesor, materia) {
    var funcion = "solicitar_prestamo";
    var cadena = "cantidad=" + cantidad +
        "&id=" + id +
        "&profesor=" + profesor +
        "&materia=" + materia +
        "&funcion=" + funcion;
    $.ajax({
        type: "POST",
        url: "funciones_alumno.php",
        data: cadena,
        success: function (response) {
            alert(response);
        }
    });

}

function materias(funcion) {

    $.ajax({
        type: "POST",
        url: "funciones_alumno.php",
        data: "funcion=" + funcion,
        success: function (response) {
            var obj = JSON.parse(response);
            $("#materia").empty();
            $("#materia").append(" <option>Selecciona una opción</option>");
            $.each(obj, function (i, value) {
                $("#materia").append('<option value="' + obj[i].id_materia + '">' + obj[i].nombre_materia + '</option>');
            });
        }
    });
}

function profesores(funcion) {

    $.ajax({
        type: "POST",
        url: "funciones_alumno.php",
        data: "funcion=" + funcion,
        success: function (response) {
            var obj = JSON.parse(response);
            $("#profesor").empty();
            $("#profesor").append(" <option>Selecciona una opción</option>");
            $.each(obj, function (i, value) {
                $("#profesor").append('<option value="' + obj[i].id_docente + '">' + obj[i].nombre + ' ' + obj[i].apellido_paterno + ' ' + obj[i].apellido_materno + '</option>');
            });
        }
    });
}