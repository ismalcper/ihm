function login_administrador(funcion) {
    /*  proposito: verificar al usuario como administrador
        precondiciones: 
            1) La variable funcion guarda el nombre del case en php 
            2) Los campos en cuenta y contraseña no estan vacios
        poscondiciones: 
            1) Permitimos el acceso a la pagina index.php de administradores
    */
    var cuenta = $("#cuenta").val();
    var contrasena = $("#contrasena").val();
    var cadena = "cuenta=" + cuenta + "&contrasena=" + contrasena + "&funcion=" + funcion;
    $.ajax({
        type: "POST",
        url: "funciones_administrador.php",
        data: cadena,
        success: function (response) {

            if (response == "true") {
                location.href = "index.php";
            } else {
                alert("Error de credenciales");
            }
        }
    });
}

function carreras(funcion) {
    /*  proposito: buscar todas las carrerar que se tienen en la base
        precondiciones: 
            1) La variable funcion guarda el nombre del case en php 
        poscondiciones: 
            1) Agregamos las carrera al select de carreras dentro del formulario
    */
    $.ajax({
        type: "POST",
        url: "funciones_administrador.php",
        data: "funcion=" + funcion,
        success: function (response) {
            var obj = JSON.parse(response);
            $("#carreras").empty();
            $("#carreras").append(" <option>Selecciona una opción</option>");
            $.each(obj, function (i, value) {
                $("#carreras").append('<option value="' + obj[i].id_carrera + '">' + obj[i].nombre_carrera + '</option>');
            });
        }
    });
}

function registrarAlumno(funcion) {
    /*  proposito: Registrar a un nuevo alumno
        precondiciones: 
            1) La variable funcion guarda el nombre del case en php 
        poscondiciones: 
            1) Agregamos el nuevo alumno
    */
    var cuenta = $("#cuenta").val();
    var nombre = $("#nombre").val();
    var apellido_paterno = $("#apellido_paterno").val();
    var apellido_materno = $("#apellido_materno").val();
    var semestre = $("#semestre").val();
    var carrera = $("#carreras").val();
    var cadena = "cuenta=" + cuenta +
        "&nombre=" + nombre +
        "&apellido_paterno=" + apellido_paterno +
        "&apellido_materno=" + apellido_materno +
        "&semestre=" + semestre +
        "&carrera=" + carrera +
        "&funcion=" + funcion;
    $.ajax({
        type: "POST",
        url: "funciones_administrador.php",
        data: cadena,
        success: function (response) {
            alert(response);
        }
    });
}

function registrarMaterial(funcion) {
    /*  proposito: Registrar a un nuevo material
        precondiciones: 
            1) La variable funcion guarda el nombre del case en php 
        poscondiciones: 
            1) Agregamos el nuevo material
    */

    var nombre = $("#nombre").val();
    var descripcion = $("#descripcion").val();
    var cantidad = $("#cantidad").val();

    var cadena = "nombre=" + nombre +
        "&descripcion=" + descripcion +
        "&cantidad=" + cantidad +
        "&funcion=" + funcion;
    $.ajax({
        type: "POST",
        url: "funciones_administrador.php",
        data: cadena,
        success: function (response) {
            alert(response);
        }
    });
}

function aprobarMaterial(funcion) {
    var cuenta = $("#cuenta").val();

    var cadena = "cuenta=" + cuenta +
        "&funcion=" + funcion;
    $.ajax({
        type: "POST",
        url: "funciones_administrador.php",
        data: cadena,
        success: function (response) {

            alert(response);
        }
    });
}