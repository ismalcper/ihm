<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../fontawesome/css/all.min.css">
    <link rel="stylesheet" href="../bootstrap-4.3.1/css/bootstrap.css">
    <link rel="stylesheet" href="../css/styles.css">



    <!-- CSS -->
    <link rel="stylesheet" href="../alertifyjs/css/alertify.min.css" />
    <!-- Default theme -->
    <link rel="stylesheet" href="../alertifyjs/css/themes/default.min.css" />
    <!-- Semantic UI theme -->
    <link rel="stylesheet" href="../alertifyjs/css/themes/semantic.min.css" />
    <!-- Bootstrap theme -->
    <link rel="stylesheet" href="../alertifyjs/css/themes/bootstrap.min.css" />
    <title>Inicio</title>
</head>

<body>
    <img src="../img/bannerFI.png" alt="" srcset="">
    <nav class="navbar navbar-expand-lg  barra">


        <div class="collapse navbar-collapse  nav-tabs">
            <ul class="navbar-nav mr-auto">
                <a href="" class="nav-link active">Sitema de control para laboratorios</a>
            </ul>
            <form class="form-inline my-2 my-lg-0">
                <a class="nav-link" href="../index.php">Soy Alumno</a>
            </form>
        </div>
    </nav>
    <div class=" inicio">
        <div class="container " align="center">
            <div class="row">
                <div class="col-md-8">

                </div>
                <div class="col-md-4 formulario">
                    <div class="row">
                        <div class="form-group">
                            <img src="../img/usuario.png" alt="" srcset="">
                            <div class="col-md-12">
                                <input type="text" class="form-control" name="cuenta" id="cuenta"
                                    aria-describedby="helpId" placeholder="Número de cuenta">
                            </div>
                            <div class="col-md-12">
                                <input type="password" class="form-control" name="contrasena" id="contrasena"
                                    aria-describedby="helpId" placeholder="Contraseña">
                            </div>
                            <br>
                            <div class="row">

                                <div class="col-md-12" aling="center">
                                    <a class="btn btn-primary" href="" id="login" name="login" role="button">Iniciar
                                        sesión</a>
                                </div>
                            </div>



                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
        <br>
        <br>
        <div id="footer">
        </div>

        <script src="../bootstrap-4.3.1/js/bootstrap.js"></script>
        <script src="../js/jquery-3.4.1.js"></script>
        <script src="../js/funciones_administrador.js"></script>
        <script>
        $(document).ready(function() {
            $("#footer").load("../footer.html");
            $("#login").click(function() {
                login_administrador("login")
            });



        });
        </script>
</body>

</html>