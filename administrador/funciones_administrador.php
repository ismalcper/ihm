<?php
require "../conexion.php";
//Resivimos la variable "funcion" por POST y así determinamos a a que funcion llamar
$funcion = $_POST['funcion'];
//seleccionamos la opción
switch ($funcion) {
    case 'login':
        //Extraemos los datos para el login
        $cuenta = $_POST['cuenta'];
        $contrasena = $_POST['contrasena'];
        $respuesta = login($cuenta,$contrasena);
      //Si el usuario esta registrado iniciamos una nueva sesion e indicamos que tiene acceso
       if($respuesta!=""){
        session_start();
         $_SESSION['administrador']=$respuesta;
        echo "true";
       }else{
        echo "false";
       }
        
    break;
    case 'carreras':
        $carr=carreras();
        echo $carr;
    break;
    case 'registrarAlumno':
         //Extraemos los datos para el registro del nuevo alumno
        $cuenta = $_POST['cuenta'];
        $nombre = $_POST['nombre'];
        $apellido_paterno = $_POST['apellido_paterno'];
        $apellido_materno = $_POST['apellido_materno'];
        $semestre = $_POST['semestre'];
        $carrera = $_POST['carrera'];
        $carr=registrarAlumno($cuenta,$nombre,$apellido_paterno,$apellido_materno,$semestre,$carrera);
        echo $carr;
    break;
    case 'registrarMaterial':
        //Extraemos los datos para el registro del nuevo material
       $nombre = $_POST['nombre'];
       $descripcion = $_POST['descripcion'];
       $cantidad = $_POST['cantidad'];
       $res=registrarMaterial($nombre,$descripcion,$cantidad);
       echo $res;
   break;
   case 'aprobarMaterial':
        //Extraemos los datos para el registro del nuevo material
        $cuenta = $_POST['cuenta'];
        $res=aprobarMaterial($cuenta);
        echo $res;
   break;
}
function login($cuenta,$contrasena){
    $conn=conectar();
    $sql = "SELECT id_administrador,contrasena FROM administrador INNER JOIN persona ON persona.id_persona=administrador.id_persona where id_administrador ='$cuenta' and contrasena='$contrasena'";
    $result=mysqli_query($conn,$sql); 
    $id="";
     while ($row=mysqli_fetch_array($result)) {
        if($row["id_administrador"]!=""){
            $id=$row["id_administrador"];
        }
     }
    $conn->close();
    return $id;
}
function carreras(){
    
    $conn=conectar();
    $sql = "SELECT id_carrera,nombre_carrera FROM carrera";
    $result=mysqli_query($conn,$sql); 
     while($row = mysqli_fetch_array($result)){
        $carreras[] = array('id_carrera'=> $row["id_carrera"], 'nombre_carrera'=> $row["nombre_carrera"]);
     }
    $conn->close();
    return json_encode($carreras);
}
function registrarAlumno($cuenta,$nombre,$apellido_paterno,$apellido_materno,$semestre,$carrera){
    
    $conn=conectar();
    $numeroRow=0;
    $sql="SELECT cuenta FROM alumno WHERE cuenta='$cuenta'";
    $numeroRow=$numeroRow+ mysqli_num_rows(mysqli_query($conn,$sql));
    
    if($numeroRow>0) { 
        $conn->close();
        return "Usuario ya registrado";
    }else{
        $clavePersona= "PER006";//mysqli_insert_id($conn);
        $sql = "INSERT INTO persona (id_persona,nombre,apellido_paterno,apellido_materno,contrasena) VALUES ('$clavePersona','$nombre','$apellido_paterno','$apellido_materno','$cuenta')";
        if (mysqli_query($conn,$sql)){
            $sql = "INSERT INTO alumno (cuenta,id_persona,id_carrera,semestre) VALUES ('$cuenta','$clavePersona','$carrera','$semestre')";
            if (mysqli_query($conn,$sql)){
                $conn->close();
                return "¡Registrado con Exito!";
            }
        }
    }  
    $conn->close();
    return "Error";
}
function registrarMaterial($nombre,$descripcion,$cantidad){
    $conn=conectar();
    $claveMaterial= "MTR003";//mysqli_insert_id($conn);
    $sql = "INSERT INTO material (id_material,nombre_materiaL,descripcion,cantidad_existente) VALUES ('$claveMaterial','$nombre','$descripcion',$cantidad);";
    if (mysqli_query($conn,$sql)){
            $conn->close();
            return "¡Registrado con Exito!";   
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }
    $conn->close();
    return "Error";
}
function aprobarMaterial($cuenta){
    $conn=conectar();
    
    $sql = "SELECT * FROM prestamo_material INNER JOIN prestamo 
    ON prestamo_material.id_prestamo=prestamo.id_prestamo INNER JOIN material ON prestamo_material.id_material = material.id_material 
    INNER JOIN alumno ON prestamo.cuenta_alumno=alumno.cuenta WHERE alumno.cuenta = '$cuenta';";
    $resultado=mysqli_query($conn,$sql);
    if ($resultado){
            $texto= "<table>";
        while($mostrar = mysqli_fetch_array($resultado)){
            $texto.= "<tr>";   
            $texto.= "<td>" . $mostrar['id_material'] . "</td>";
            $texto.= "<td>" . $mostrar['nombre_material'] . "</td>";
            $texto.= "<td>" . $mostrar['descripción'] . "</td>";
            $texto.= "</tr>";
        }
        $texto.= "</table>";
        $conn->close();
        return $texto;   
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }
    $conn->close();
    return "Error";
}
?>