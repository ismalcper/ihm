<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laboratorio IHM</title>
    <link rel="stylesheet" href="../css/styles.css">
    <link rel="stylesheet" href="../fontawesome/css/all.min.css">
    <link rel="stylesheet" href="../bootstrap-4.3.1/css/bootstrap.css">

</head>

<body>
    <img src="../img/bannerFI.png" alt="" srcset="">
    <nav class="navbar navbar-expand-lg  barra ">


        <div class="collapse navbar-collapse nav-tabs">
            <ul class="navbar-nav mr-auto ">
                <li class="nav-item">
                    <a class="nav-link " href="index.php">Home</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link active" href="nuevoAlumno.php">Registrar nuevo alumno</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Registrar nuevo docente</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="nuevoMaterial.php">Registrar nuevo material</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Entregar material</a>
                </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">
                <a class="nav-link" href="../index.php">Salir</a>
            </form>
        </div>
    </nav>
    <div>
        <div class="container" align="center">
            <div class="row">
                <div class="col-md-2">

                </div>
                <div class="col-md-8 registro">
                    <div class="row ">
                        <img src="../img/usuario.png" alt="" srcset="">
                    </div>
                    <div class="form-group">
                        <label for="">Registro de un nuevo alumno</label>
                        <div class="input-group-append">
                            <span class="input-group-text"><i class="fa fa-key" aria-hidden="true"></i></span>
                            <input type="text" name="cuenta" id="cuenta" class="form-control"
                                placeholder="Número de cuenta" aria-describedby="helpId">
                        </div>
                        <small class="text-muted">Número de cuenta</small>
                        <div class="input-group-append">
                            <span class="input-group-text"><i class="fa fa-user" aria-hidden="true"></i></span>
                            <input type="text" name="nombre" id="nombre" class="form-control" placeholder="Nombre"
                                aria-describedby="helpId">
                        </div>
                        <small class="text-muted">Nombre</small>
                        <div class="input-group-append">
                            <span class="input-group-text"><i class="fa fa-user" aria-hidden="true"></i></span>
                            <input type="text" name="apellido_paterno" id="apellido_paterno" class="form-control"
                                placeholder="Apellido paterno" aria-describedby="helpId">
                        </div>
                        <small class="text-muted">Apellido paterno</small>
                        <div class="input-group-append">
                            <span class="input-group-text"><i class="fa fa-user" aria-hidden="true"></i></span>
                            <input type="text" name="apellido_materno" id="apellido_materno" class="form-control"
                                placeholder="Apellido materno" aria-describedby="helpId">
                        </div>
                        <small class="text-muted">Apellido materno</small>
                        <!-- <div class="input-group-append">
                            <span class="input-group-text"><i class="fa fa-unlock-alt" aria-hidden="true"></i></span>
                            <input type="password" name="" id="" class="form-control" placeholder="Contraseña"
                                aria-describedby="helpId">
                        </div>
                        <small id="helpId" class="text-muted">Contraseña</small> -->

                        <div class="row">
                            <div class="col-md-4">
                                <select class="form-control" name="semestre" id="semestre">
                                    <option>Selecciona una opción</option>
                                    <option value="1">1°</option>
                                    <option value="2">2°</option>
                                    <option value="3">3°</option>
                                    <option value="4">4°</option>
                                    <option value="5">5°</option>
                                    <option value="6">6°</option>
                                    <option value="7">7°</option>
                                    <option value="8">8°</option>
                                    <option value="9">9°</option>
                                    <option value="10">10°</option>
                                    <option value="11">11°</option>
                                    <option value="12">12°</option>
                                    <option value="13">13°</option>
                                    <option value="14">14°</option>
                                    <option value="15">15°</option>
                                    <option value="16">16°</option>
                                    <option value="17">17°</option>
                                    <option value="18">18°</option>
                                </select>
                                <small id="helpId" class="text-muted">Semestre</small>
                            </div>
                            <div class="col-md-8">
                                <select class="form-control" name="carreras" id="carreras">
                                    <option>Selecciona una opción</option>
                                </select>
                                <small id="helpId" class="text-muted">Licenciatura</small>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12" aling="center">
                                <a class="btn btn-primary" href="" id="registrar" name="registrar"
                                    role="button">Registrar alumno</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
        <br>
        <br>
        <div id="footer">
        </div>
    </div>

    <script src="../bootstrap-4.3.1/js/bootstrap.js"></script>
    <script src="../js/jquery-3.4.1.js"></script>
    <script src="../js/funciones_administrador.js"></script>
    <script>
    $(document).ready(function() {
        $("#footer").load("../footer.html");
        carreras("carreras");
        $("#registrar").click(function(e) {
            registrarAlumno("registrarAlumno");
        });

    });
    </script>
</body>

</html>