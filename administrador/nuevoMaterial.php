<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laboratorio IHM</title>
    <link rel="stylesheet" href="../css/styles.css">
    <link rel="stylesheet" href="../fontawesome/css/all.min.css">
    <link rel="stylesheet" href="../bootstrap-4.3.1/css/bootstrap.css">

</head>

<body>
    <img src="../img/bannerFI.png" alt="" srcset="">
    <nav class="navbar navbar-expand-lg  barra ">


        <div class="collapse navbar-collapse nav-tabs">
            <ul class="navbar-nav mr-auto ">
                <li class="nav-item">
                    <a class="nav-link " href="index.php">Home</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link " href="nuevoAlumno.php">Registrar nuevo alumno</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="#">Registrar nuevo docente</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="nuevoMaterial.php">Registrar nuevo material</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Entregar material</a>
                </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">
                <a class="nav-link" href="../index.php">Salir</a>
            </form>
        </div>
    </nav>
    <div>
        <div class="container" align="center">
            <div class="row">
                <div class="col-md-2">

                </div>
                <div class="col-md-8 registro">
                    <div class="row ">
                        <img src="../img/carpeta.png" alt="" srcset="">
                    </div>
                    <div class="form-group">
                        <label for="">Registro de un nuevo material</label>

                        <div class="input-group-append">
                            <span class="input-group-text"><i class="fas fa-toolbox"></i></span>
                            <input type="text" name="nombre" id="nombre" class="form-control"
                                placeholder="Nombre del material" aria-describedby="helpId">
                        </div>
                        <small id="helpId" class="text-muted">Nombre</small>
                        <div class="input-group-append">

                            <span class="input-group-text"><i class="fas fa-comments"></i></span>
                            <textarea class="form-control" name="descripcion" id="descripcion" rows="3"></textarea>

                        </div>
                        <small id="helpId" class="text-muted">Descripción</small>
                        <div class="input-group-append">
                            <span class="input-group-text"><i class="fa fa-user" aria-hidden="true"></i></span>
                            <input type="number" min="0" name="cantidad" id="cantidad" class="form-control"
                                placeholder="Apellido materno" aria-describedby="helpId">
                        </div>
                        <small id="helpId" class="text-muted">Cantidad</small>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12" aling="center">
                            <a class="btn btn-primary" href="" id="registrar_material" name="registrar_material"
                                role="button">Registrar material</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <br>
    <br>
    <br>
    <div id="footer">
    </div>
    </div>

    <script src="../bootstrap-4.3.1/js/bootstrap.js"></script>
    <script src="../js/jquery-3.4.1.js"></script>
    <script src="../js/funciones_administrador.js"></script>
    <script>
    $(document).ready(function() {
        $("#footer").load("../footer.html");
        $("#registrar_material").click(function(e) {
            registrarMaterial("registrarMaterial");
        });
    });
    </script>
</body>

</html>