<?php
require "../conexion.php";
session_start();
if(isset($_SESSION['administrador'])){
$admin=$_SESSION['administrador'];
   
}else{
    header('Location: ../index.php');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laboratorio IHM</title>
    <link rel="stylesheet" href="../fontawesome/css/all.min.css">
    <link rel="stylesheet" href="../bootstrap-4.3.1/css/bootstrap.css">
    <link rel="stylesheet" href="../css/styles.css">
</head>

<body>
    <img src="../img/bannerFI.png" alt="" srcset="">
    <nav class="navbar navbar-expand-lg  barra ">
        <div class="collapse navbar-collapse nav-tabs">
            <ul class="navbar-nav mr-auto ">
                <li class="nav-item">
                    <a class="nav-link active" href="index.php">Home</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="nuevoAlumno.php">Registrar nuevo alumno</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Registrar nuevo docente</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Registrar nueva materia</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="nuevoMaterial.php">Registrar nuevo material</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="aprobarMaterial.php">Entregar material</a>
                </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">
                <a class="nav-link" href="">Mi cuenta</a>
                <a class="nav-link" href="../cerrar.php">Salir</a>
            </form>
        </div>
    </nav>
    <div>
        <div class="container" align="center">
            <div class="row">
                <div class="col-md-4 formulario">
                    <?php
                        $sql = "SELECT * FROM administrador INNER JOIN persona ON persona.id_persona=administrador.id_persona where id_administrador ='$admin'";
                        $conn=conectar();
                        $result=mysqli_query($conn,$sql); 
                        while ($row=mysqli_fetch_array($result)) {
                        echo '<div class="row">
                            <img src="../img/usuario.png" alt="" srcset="">
                        </div>
                        <div class="row">
                            <h3>No. Cuenta: '.$row['id_administrador'].'</h3>
                        </div>
                        <br>
                        <div class="row">
                            <h5>Nombre: '.$row['nombre'].' '.$row['apellido_paterno'].' '.$row['apellido_materno'].'</h5>
                        </div>
                        <br>
                        <div class="row">
                            <h5>Profesión: '.$row['profesion'].'</h5>
                        </div>
                        <br>
                        <div class="row">
                            <h5>Grado Academico: '.$row['grado_academico'].'</h5>
                        </div>';
                
                         }
                        $conn->close();
          
                    ?>

                </div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6 botones-admin">
                            <a name="" id="" class="btn " href="nuevoAlumno.php" role="button">Registrar alumno</a>
                        </div>
                        <div class="col-md-6 botones-admin">
                            <a name="" id="" class="btn" href="#" role="button">Registrar docente</a>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-6 botones-admin">
                            <a name="" id="" class="btn " href="nuevoMaterial.php" role="button">Registrar material</a>
                        </div>
                        <div class="col-md-6 botones-admin">
                            <a name="" id="" class="btn" href="aprobarMaterial.php" role="button"> Aprovar material</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
        <br>
        <br>
        <div id="footer">
        </div>
    </div>

    <script src="../bootstrap-4.3.1/js/bootstrap.js"></script>
    <script src="../js/jquery-3.4.1.js"></script>
    <script>
    $(document).ready(function() {
        $("#footer").load("../footer.html");
    });
    </script>
</body>

</html>