<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laboratorio IHM</title>
    <link rel="stylesheet" href="../css/styles.css">
    <link rel="stylesheet" href="../fontawesome/css/all.min.css">
    <link rel="stylesheet" href="../bootstrap-4.3.1/css/bootstrap.css">

</head>

<body>
    <img src="../img/bannerFI.png" alt="" srcset="">
    <nav class="navbar navbar-expand-lg  barra ">


        <div class="collapse navbar-collapse nav-tabs">
            <ul class="navbar-nav mr-auto ">
                <li class="nav-item">
                    <a class="nav-link " href="index.php">Home</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link " href="nuevoAlumno.php">Registrar nuevo alumno</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="#">Registrar nuevo docente</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="nuevoMaterial.php">Registrar nuevo material</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="aprobarMaterial.php">Aprobar material</a>
                </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">
                <a class="nav-link" href="../index.php">Salir</a>
            </form>
        </div>
    </nav>
    <div>
        <div class="container" align="center">
            <div class="row">
                <div class="col-md-2">

                </div>
                <div class="col-md-8 registro">
                    <div class="row ">
                        <img src="../img/comprobado.png" alt="" srcset="">
                    </div>
                    <div class="form-group">
                        <label for="">Aprobar material</label>
                        <div class="input-group-append">
                            <span class="input-group-text"><i class="fa fa-key" aria-hidden="true"></i></span>
                            <input type="text" name="cuenta" id="cuenta" class="form-control"
                                placeholder="Número de cuenta" aria-describedby="helpId">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12" aling="center">
                            <a class="btn btn-primary" href="" id="aprobar_material" name="aprobar_material"
                                role="button">Buscar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <br>
    <br>
    <br>
    <div id="footer">
    </div>
    </div>

    <script src="../bootstrap-4.3.1/js/bootstrap.js"></script>
    <script src="../js/jquery-3.4.1.js"></script>
    <script src="../js/funciones_administrador.js"></script>
    <script>
    $(document).ready(function() {
        $("#footer").load("../footer.html");
        $("#aprobar_material").click(function(e) {
            aprobarMaterial("aprobarMaterial");
        });
    });
    </script>
</body>

</html>